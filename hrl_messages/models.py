from django.db import models
from hrl_users.models import HrlUser

class HrlMessage(models.Model):
    subject = models.CharField(max_length=50, blank=True, null=True)
    message = models.CharField(max_length=300, blank=True, null=True)

    sender = models.ForeignKey(HrlUser, on_delete=models.SET_NULL, related_name='sent_messages', blank=True, null=True)
    receiver = models.ForeignKey(HrlUser, on_delete=models.SET_NULL, related_name='received_messages', blank=True, null=True)

    read = models.BooleanField(default=False, blank=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


    def __unicode__(self):
        return u'{}'.format(self.subject)

    def __str__(self):
        return self.subject

    class Meta:
        ordering = ['created']
        verbose_name = 'Herolo Message'
        verbose_name_plural = 'Herolo Messages'

        # 'subject', 'message', 'sender', 'receiver', 'read', 'created',