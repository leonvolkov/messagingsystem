from django.apps import AppConfig


class HrlMessagesConfig(AppConfig):
    name = 'hrl_messages'
    verbose_name = 'Herolo Messages'
