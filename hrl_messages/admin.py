from django.contrib import admin
from hrl_users.models import HrlUser
from .models import HrlMessage


class HrlMessageAdmin(admin.ModelAdmin):
    fields = ('subject', 'message', 'sender', 'receiver', 'read')
    list_display = ('subject', 'message', 'sender', 'receiver', 'read', 'created', 'updated')
    list_filter = ('read',)
    search_fields = ('subject', 'message', 'sender__email', 'sender__first_name', 'sender__last_name',
                     'receiver__email', 'receiver__first_name', 'receiver__last_name',)
    ordering = ['created', 'subject']

    def get_readonly_fields(self, request, obj=None):
        # return ['subject', 'message', 'sender', 'receiver', 'created', ]
        return ['created', ]

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name in ['sender', 'receiver',]:
            kwargs["queryset"] = HrlUser.objects.order_by('first_name','last_name','email',)
        return super(HrlMessageAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    # 'subject', 'message', 'sender', 'receiver', 'read', 'created', 'updated',

admin.site.register(HrlMessage, HrlMessageAdmin)