from rest_framework import serializers
from rest_framework_extensions.fields import ResourceUriField

from hrl_messages.models import HrlMessage
from hrl_users.models import HrlUser
from main.constants import API_DATETIME_FORMAT

class HrlMessageSerializer(serializers.HyperlinkedModelSerializer):
    sender = serializers.HyperlinkedRelatedField(view_name='hrluser-detail', read_only=True)
    receiver = serializers.HyperlinkedRelatedField(view_name='hrluser-detail', queryset = HrlUser.objects.all())
    created = serializers.DateTimeField(format=API_DATETIME_FORMAT, read_only=True)
    updated = serializers.DateTimeField(format=API_DATETIME_FORMAT, read_only=True)
    resource_uri = ResourceUriField(view_name='hrlmessage-detail', read_only=True)
    subject = serializers.CharField(required=True)

    def create(self, validated_data):
        return HrlMessage.objects.create(sender=self.context['request'].user, **validated_data)

    class Meta:
        model = HrlMessage
        fields = ('subject', 'message', 'sender', 'receiver', 'read', 'created', 'updated', 'resource_uri',)
        read_only_fields = ('read',)

class HrlMessageReadOnlyUsersSerializer(HrlMessageSerializer):
    class Meta(HrlMessageSerializer.Meta):
        read_only_fields = ('sender', 'receiver',)

# 'subject', 'message', 'sender', 'receiver', 'read', 'created', 'updated'