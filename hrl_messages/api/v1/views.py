from django.db.models import Q
from rest_framework import viewsets, status
from rest_framework.response import Response

from hrl_messages.models import HrlMessage
from hrl_messages.api.v1.serializers import HrlMessageSerializer, HrlMessageReadOnlyUsersSerializer

class HrlMessagesViewSet(viewsets.ModelViewSet):
    serializer_class = HrlMessageSerializer

    def get_queryset(self):
        """
        This view should return a list of all the messages
        for the currently authenticated user.
        """
        user = self.request.user
        queryset = HrlMessage.objects
        view_unread = self.request.query_params.get('unread', None)
        view_tome = self.request.query_params.get('tome', None)
        view_my = self.request.query_params.get('my', None)

        if user.is_superuser or user.is_staff:
            queryset = queryset.all()
        else:
            queryset = queryset.filter(Q(sender=user) | Q(receiver=user))

        if not (view_unread is None):
            queryset = queryset.filter(read=False)
        if not (view_tome is None):
            queryset = queryset.filter(receiver=user)
        if not (view_my is None):
            queryset = queryset.filter(sender=user)

        return queryset.order_by('created','subject',)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        user = self.request.user
        # Auto set read flag to true after details view by receiver
        if instance.receiver.pk == user.pk:
            instance.read = True
            instance.save()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def get_serializer_class(self):
        serializer_class = self.serializer_class
        if self.request.method in ['PUT', 'PATCH']:
            serializer_class = HrlMessageReadOnlyUsersSerializer
        return serializer_class

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        user = self.request.user
        if instance.sender.pk == user.pk or instance.receiver.pk == user.pk or user.is_superuser or user.is_staff:
            self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response({"You must be sender, receiver or an admin to perform this operation"}, status=status.HTTP_403_FORBIDDEN)

# 'subject', 'message', 'sender', 'receiver', 'read', 'created', 'updated'