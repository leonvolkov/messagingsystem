from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers
from rest_framework_extensions.fields import ResourceUriField

from hrl_users.models import HrlUser

class HrlUserSerializer(serializers.HyperlinkedModelSerializer):
    sent_messages = serializers.HyperlinkedRelatedField(view_name= 'hrlmessage-detail', many=True, read_only=True,)
    received_messages = serializers.HyperlinkedRelatedField(view_name= 'hrlmessage-detail', many=True, read_only=True,)
    resource_uri = ResourceUriField(view_name='hrluser-detail', read_only=True)

    class Meta:
        model = HrlUser
        fields = ('username', 'first_name', 'last_name', 'email', 'sent_messages', 'received_messages', 'resource_uri',)
        read_only_fields = ('username', 'first_name', 'last_name', 'email',)

class HrlUserCrteateSerializer(HrlUserSerializer):
    email = serializers.EmailField()
    password = serializers.CharField(write_only = True, required=True)

    def create(self, validated_data):
        user = super().create(validated_data)
        # set_password also hashes the password that the user will get
        user.set_password(validated_data['password'])
        user.save()
        return user

    class Meta(HrlUserSerializer.Meta):
        fields = (*HrlUserSerializer.Meta.fields, 'password',)
        read_only_fields = ()

class HrlUserChangePasswordSerializer(HrlUserCrteateSerializer):
    new_password = serializers.CharField(write_only = True, required=True)

    def validate_new_password(self, value):
        validate_password(value)
        return value

    class Meta(HrlUserCrteateSerializer.Meta):
        fields = (*HrlUserCrteateSerializer.Meta.fields, 'new_password',)
        read_only_fields = ('username',)


# 'username', 'first_name', 'last_name', 'email', 'sent_messages', 'received_messages',