from django.core.serializers import get_serializer
from django.contrib.auth import update_session_auth_hash
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from hrl_users.models import HrlUser
from hrl_users.api.v1.serializers import HrlUserSerializer, HrlUserCrteateSerializer, HrlUserChangePasswordSerializer

class HrlUsersViewSet(viewsets.ModelViewSet):
    serializer_class = HrlUserCrteateSerializer
    queryset = HrlUser.objects.all().order_by('first_name','last_name','email',)
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self):
        if self.action == 'list':
            return HrlUserCrteateSerializer
        if self.action in ['retrieve', 'update', 'partial_update', 'delete']:
            instance = self.get_object()
            user = self.request.user
            if instance.pk == user.pk or user.is_superuser or user.is_staff:
                return HrlUserChangePasswordSerializer
            else:
                return HrlUserSerializer
        return self.serializer_class

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(data=request.data, partial=True)

        for key, value in request.data.items():
            if key in serializer.Meta.read_only_fields:
                return Response({key: ["read only value"]}, status=status.HTTP_400_BAD_REQUEST)

        if serializer.is_valid():
            if serializer.validated_data.get('password', None):
                # Check old password
                if not instance.check_password(serializer.validated_data.get('password')):
                    return Response({"password": ["wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
                # set_password also hashes the password that the user will get
                instance.set_password(serializer.validated_data.get('new_password'))

            for key, value in serializer.validated_data.items():
                if key not in ['new_password', 'password']:
                    setattr(instance, key, value)
            instance.save()
            user = self.request.user

            if instance.pk == user.pk:
                update_session_auth_hash(request, instance)

            return Response({"data": ["changed successfuly"]}, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        user = self.request.user
        if instance.pk == user.pk or user.is_superuser or user.is_staff:
            self.perform_destroy(instance)
            return Response(status=status.HTTP_204_NO_CONTENT)
        return Response({"You must be this user or an admin to perform this operation"}, status=status.HTTP_403_FORBIDDEN)