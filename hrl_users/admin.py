from django.contrib import admin

from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin

from .forms import HrlUserCreationForm, HrlUserChangeForm
from .models import HrlUser

class HrlUserAdmin(UserAdmin):
    add_form = HrlUserCreationForm
    form = HrlUserChangeForm
    model = HrlUser
    list_display = ['email', 'username', 'first_name', 'last_name',]

admin.site.register(HrlUser, HrlUserAdmin)