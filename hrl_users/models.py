from django.db import models
from django.core.validators import EmailValidator

from django.contrib.auth.models import AbstractUser

class HrlUser(AbstractUser):
    username = models.CharField(unique=True, max_length=50, blank=True, null=True)
    first_name = models.CharField(max_length=20, blank=True, null=True)
    last_name = models.CharField(max_length=20, blank=True, null=True)
    email = models.CharField(unique=True, max_length=50, blank=True, null=True, validators=[EmailValidator])

    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.get_full_name()

    def __str__(self):
        return self.email

    def get_full_name(self):
        '''
        Returns the first_name plus the last_name, with a space in between
        or email if there no names.
        '''
        if self.first_name or self.last_name:
            full_name = u'{0} {1}'.format(self.first_name, self.last_name).strip()
        else:
            full_name = u'{}'.format(self.email)
        return full_name


    class Meta:
        ordering = ['email']
        verbose_name = 'Herolo User'
        verbose_name_plural = 'Herolo Users'

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    # 'username', 'first_name','last_name','email',
