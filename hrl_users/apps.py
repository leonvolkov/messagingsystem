from django.apps import AppConfig


class HrlUsersConfig(AppConfig):
    name = 'hrl_users'
    verbose_name = 'Herolo Users'
