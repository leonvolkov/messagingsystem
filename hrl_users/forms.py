from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import HrlUser

class HrlUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm):
        model = HrlUser
        fields = ('username', 'email', 'first_name', 'last_name')

class HrlUserChangeForm(UserChangeForm):

    class Meta(UserChangeForm):
        model = HrlUser
        fields = ('username', 'email', 'first_name', 'last_name')