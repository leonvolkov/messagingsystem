from rest_framework.routers import DefaultRouter
from hrl_users.api.v1.views import HrlUsersViewSet
from hrl_messages.api.v1.views import HrlMessagesViewSet


router = DefaultRouter()

users_router = router.register(r'hrl_users', HrlUsersViewSet, base_name='hrluser')
messages_router = router.register(r'hrl_messages', HrlMessagesViewSet, base_name='hrlmessage')
