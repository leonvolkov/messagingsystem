from collections import OrderedDict
from rest_framework import pagination
from rest_framework.response import Response


class HrlCustomPagination(pagination.PageNumberPagination):
    page_size = 20
    page_size_query_param = 'per_page'
    max_page_size = 10000

    def get_paginated_response(self, data):
        resp = OrderedDict([
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('count', self.page.paginator.count),
            ('num_pages', self.page.paginator.num_pages),
            ('per_page', self.page.paginator.per_page),
            ('page', self.page.number),
            ('results', data),
        ])
        return Response(resp)