from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from rest_framework.utils import json


@login_required
def index(request, *args, **kwargs):
    user = request.user
    initial_data = {}
    initial_data['user'] = {
                'full_name': user.get_full_name(),
                'user_id': user.pk,
                'username': user.username,
                'email': user.email,
                'is_staff': user.is_staff,
                'is_superuser': user.is_superuser,
            }
    ctx = {
        "initial_data": initial_data,
    }

    return render(request, 'index.html', ctx, content_type="text/html")