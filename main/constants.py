"""
main app constants
"""

# Date format for API
API_DATE_FORMAT = '%Y-%m-%d'
API_DATETIME_FORMAT = API_DATE_FORMAT + ' %H:%M:%S'

# String default
NONE_STRING = 'N/A'
